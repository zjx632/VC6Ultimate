#pragma once

typedef struct _RECTL{
    LONG left;
    LONG top;
    LONG right;
    LONG bottom;
} 	RECTL;

typedef struct _RECTL* PRECTL;

typedef struct _RECTL* LPRECTL;

typedef struct tagPOINT{
  LONG x;
  LONG y;
}	POINT;

typedef struct tagPOINT* PPOINT;

typedef struct tagPOINT* LPPOINT;

typedef struct _POINTL{
	LONG x;
	LONG y;
} 	POINTL;

typedef struct _POINTL* PPOINTL;

typedef struct tagSIZE{
	LONG cx;
	LONG cy;
} 	SIZE;

typedef struct tagSIZE *PSIZE;

typedef struct tagSIZE *LPSIZE;

typedef SIZE               SIZEL;
typedef SIZE               *PSIZEL, *LPSIZEL;

typedef struct tagSIZE* PSIZEL;

typedef struct tagSIZE* LPSIZEL;

typedef struct tagRECT
{
    LONG    left;
    LONG    top;
    LONG    right;
    LONG    bottom;
} RECT, *PRECT, NEAR *NPRECT, FAR *LPRECT;

typedef const RECT FAR* LPCRECT;

typedef const RECTL FAR* LPCRECTL;

typedef struct tagPOINTS{
    SHORT   x;
    SHORT   y;
} POINTS, *PPOINTS, *LPPOINTS;
