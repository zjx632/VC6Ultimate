@prompt $
@mkdir temp > nul
@cl /nologo /G6 /Gr /ML /W3 /O2 /Ob2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_SHOW_FPS" /D "USE_DIRECTDRAW" /I"../lib/" /I"../winlib/" /I"../stdlib/" /I"Q:\@Projets\DevTools" /Fo"C:/Temp/Release/SpiropsDemo/" /Fd"C:/Temp/Release/SpiropsDemo/" /FD /c %CPP_FILES%
@set LIB=%LIB%;Q:\DX9SDK\LIB
@link /IGNORE:4089 /nologo kernel32.lib user32.lib gdi32.lib comctl32.lib shell32.lib shlwapi.lib ddraw.lib dxguid.lib dsound.lib msvcrt.lib Advapi32.lib Ole32.lib olepro32.lib uuid.lib d3d9.lib d3dx9.lib /nologo /subsystem:windows /pdb:none /machine:I386 /nodefaultlib /out:"%OUT_FILE%" /opt:nowin98 "C:\Temp\Release\SpiropsDemo\*.obj" "C:\Temp\Release\SpiropsDemo\Resources.res" "C:\Temp\R\WndMoreSimple.lib" "C:\Temp\R\Wrap2DLib.lib" "c:\temp\r\SpirOpsv5.0.lib"
@rmdir temp /S /Q > nul
@prompt $p$g

