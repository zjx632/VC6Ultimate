@prompt $
@mkdir temp > nul
@cl /nologo /Gr /MD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SOLDATBRAIN_EXPORTS" /D "_SYNDEBUG" /D "SPIROPSDLL_IMPORTS" /D "_SYNEDITOR" /I"../lib/" /I"../winlib/" /I"../stdlib/" /Fo"./temp/" /Fd"./temp/" /FD /c %CPP_FILES%
@link /IGNORE:4089 /nologo ./temp/*.obj ../lib/SpirOpsv5.0.lib Kernel32.lib msvcrt.lib /dll /pdb:none /machine:I386 /nodefaultlib /out:"../%OUT_DLLFILE%"
@del ../OutDLL/*.lib /Q
@del ../OutDLL/*.exp /Q
@rmdir temp /S /Q > nul
