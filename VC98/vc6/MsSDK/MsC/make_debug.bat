@echo off
prompt $
mkdir temp > nul
cl /nologo /MDd /W3 /Gm- /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SOLDATBRAIN_EXPORTS" /D "_SYNDEBUG" /D "SPIROPSDLL_IMPORTS" /D "_SYNEDITOR" /I"../lib/" /I"../winlib/" /I"../stdlib/" /Fo"./temp/" /Fd"./temp/" /FD /GZ /c %CPP_FILES%
set ERROR_LVL=%ERRORLEVEL%
rem return = %ERRORLEVEL%
if not %ERROR_LVL% == 0 (echo Error executing cl.exe & GOTO end)
link /nologo ./temp/*.obj ../lib/SpirOpsv5.0.lib Kernel32.lib msvcrtd.lib /dll /pdb:none /debug /machine:I386 /nodefaultlib /out:"../OutDLL/%OUT_DLLFILE%"
set ERROR_LVL=%ERRORLEVEL%
del ..\OutDLL\*.lib /Q > nul
del ..\OutDLL\*.exp /Q > nul
if not %ERROR_LVL% == 0 (echo Error executing link.exe & GOTO end)
rem echo %OUT_DLLFILE% - [OK]
:end
rmdir temp /S /Q > nul
set ERROR_LVL=
exit %ERROR_LVL%
