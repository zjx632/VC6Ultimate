c1xx.dll a �t� modifi� pour ne pas �tre limit� � 100 erreurs.

Lorsqu'il atteint 100 erreurs il affiche le message :
"error count excedds %d; stopping compilation"
C'est le message 1003 (0x3EB) des resources (le m�me message (m�me id), existe aussi dans c1.dll et c2.dll).

Code d'origine

Adresse:
104EAE40 cmp eax, 100 // 83 F8 64
...
104EAE49 jle +19      // 73 13
...
104EAE59 push 100     // 6A 64
...


J'ai remplac� jle +19 en jmp +19 // EB 13
c'est � l'offset EAE49 (voir c1xx.dif)
